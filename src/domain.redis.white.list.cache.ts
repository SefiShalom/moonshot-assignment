import { injectable } from "inversify";
import { IDomainsWhiteListCache} from './interfaces';
import { getRedisCache } from "./redis.cache";


@injectable()
export class DomainsRedisWhiteListCache implements IDomainsWhiteListCache{
    
    private cache = getRedisCache(6379);
    
    public async isLimited(domain: string): Promise<boolean> {

        let isInLimitedCache = await this.cache.get(domain);
        
        if(isInLimitedCache) return true;

        return false;

    }

    public async setTimeLimit(domain: string, expiry: number, lastPostingDate: Date){
        return await this.cache.setTemporary(domain, expiry, lastPostingDate);
    } 

    public async isWhiteListed(domain: string): Promise<boolean>{
        return this.cache.isSetMember('white_list', domain);
    }

    public async setWhiteList(domains: string[]){
        domains.forEach((domain) => {
            this.cache.addSet('white_list', domain);
        });
    } 

}

