import * as express from "express";
import {inject, injectable} from "inversify";
import {IController, IDomainDataDBService, ILooseObject, IDomainsWhiteListCache} from "./interfaces";
import { UAParser } from 'ua-parser-js';
import { IPaging, IDatesRange, IDomainData } from "./interfaces";
import  TYPES from './types';

@injectable()
export class DomainDataController implements IController{

    public path: string = '/domain-data';
    public router = express.Router();
    private _domainDataDBService: IDomainDataDBService;
    private _domainWhiteListCache: IDomainsWhiteListCache;

    constructor(
        @inject(TYPES.DomainDataDBService) private domainDataDBService: IDomainDataDBService,
        @inject(TYPES.DomainsWhiteListCache) private domainsWhiteListCache: IDomainsWhiteListCache){
        this._domainDataDBService = domainDataDBService;
        this._domainWhiteListCache = domainsWhiteListCache;
        this.intializeRoutes();
        this._domainWhiteListCache.setWhiteList(['localhost:3000' ,'localhost','0.0.0.0', '127.0.0.1:3000'])
    }

    public intializeRoutes() {
        this.router.post(this.path, this.postDomainData.bind(this));
        this.router.get(this.path, this.getDomainData.bind(this));
    }

    private async postDomainData(req: express.Request, res: express.Response): Promise<any> {

        let referraldomain: string = req.headers['referer'] || req.headers['host'] as string;

        let isWhiteListed = await this._domainWhiteListCache.isWhiteListed(referraldomain);

        if(!isWhiteListed){
            return res.status(401).send(referraldomain + " Unauthorized");
        }

        let isLimited = await this._domainWhiteListCache.isLimited(req.ip);
        if(isLimited){
            return res.status(429).send("Limited for 1 request every 30 seconds.");
        }

        let parser = new UAParser(req.get("user-agent"));
        let browserInfo: object = parser.getBrowser();
        let createdAt: Date = new Date();

        await this._domainWhiteListCache.setTimeLimit(req.ip, 30, createdAt);        

        let domainData: IDomainData = {
            domain: referraldomain,
            browser: browserInfo,
            createdAt: createdAt,
            ... req.body
        }
        
        const content = await this._domainDataDBService.storeDomainData(domainData);

        return res.send({"content": content, "statusCode": 201});
    }

    private async getDomainData(req: express.Request, res: express.Response): Promise<any> {

        let referraldomain: string = req.headers['referer'] || req.headers['host'] as string;
        
        let isWhiteListed = await this._domainWhiteListCache.isWhiteListed(referraldomain);

        if(!isWhiteListed){
            return res.status(401).send(referraldomain +" Unauthorized");
        }

        let website:string = req.query["website"] as string || "";
        let skip: number = Number(req.query["skip"]) || 0;
        let limit: number = Number(req.query["limit"]) || 0;
        let from: string = req.query["from"] as string || "";
        let until: string = req.query["until"] as string || "";

        let pagin: IPaging = {skip: skip, limit: limit}
        let fromUntilDates: IDatesRange = {from: from, until: until}
        
        const content: IDomainData[] = await this._domainDataDBService.getDomainData(website, pagin, fromUntilDates);

        let domainData: ILooseObject = {};
        domainData[website] = content;
        return res.send(domainData);
    }

}

export default DomainDataController;
