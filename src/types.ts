const TYPES = {

    Controller: Symbol("Controller"),
    DomainDataDBService: Symbol("DomainDataDBService"),
    DomainDataController: Symbol("DomainDataController"),
    DomainsWhiteListCache: Symbol("DomainsWhiteListCache"),
    IDomainData: Symbol("IDomainData")

};

export default TYPES;
