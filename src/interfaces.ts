import { Document } from "mongoose";
import { Router } from 'express';
import { inject } from "inversify";
import TYPES from './types';

interface IDomainData extends Document {
    domain: string,
    browser: object,
    createdAt: Date,
    [propName: string]: any;
}

interface ILooseObject {
    [key: string]: any
}

interface IPaging{
    skip: number;
    limit: number;
}

interface IDatesRange{
    from: string;
    until: string;
}

interface IDomainDataDBService{
    getDomainData(website: string, paging:object, fromUntilDates: object): Promise<IDomainData[]>;
    storeDomainData(data: IDomainData): Promise<IDomainData>;
}

interface IDomainsWhiteListCache{
    isLimited(ip: string): Promise<boolean>;
    isWhiteListed(domian: string): Promise<boolean>;
    setTimeLimit(ip: string, expiry: number, lastPostingDate: Date): Promise<any>;
    setWhiteList(domains: string[]): Promise<any>;
}
 
interface IController {
    path: string;
    router: Router;
}
 

export {  ILooseObject, IDomainData, IPaging, IDatesRange, IDomainDataDBService, IDomainsWhiteListCache, IController }