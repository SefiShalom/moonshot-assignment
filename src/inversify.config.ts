import { Container } from "inversify";
import TYPES from "./types";
import { IDomainDataDBService, IDomainsWhiteListCache } from "./interfaces";
import  {DomainDataMongoDBService} from './domain.data.mongo.db.service';
import  { DomainsRedisWhiteListCache } from './domain.redis.white.list.cache';
import DomainDataController from "./domain.data.controller";

var container = new Container();
container.bind<IDomainDataDBService>(TYPES.DomainDataDBService).to(DomainDataMongoDBService);
container.bind<IDomainsWhiteListCache>(TYPES.DomainsWhiteListCache).to(DomainsRedisWhiteListCache);
container.bind<DomainDataController>(TYPES.DomainDataController).to(DomainDataController);

export default container;