
import { App } from './app';
import  container from './inversify.config';
import TYPES from './types';
import DomainDataController from './domain.data.controller';
import { MongoDBConnection } from './mongo.db.connection';
import { ProcessConfigLoader } from './env';


ProcessConfigLoader.Load('./dist/.env');


const app = new App(
  [
    container.get<DomainDataController>(TYPES.DomainDataController),
  ],
);

MongoDBConnection.initConnection();
app.listen();
