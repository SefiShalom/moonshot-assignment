import redis from 'redis';
import  { promisify } from 'util';

export function getRedisCache(port: number){


    const client = redis.createClient(6379, process.env.REDIS_CONT_NAME);

    
    const rset = promisify(client.set).bind(client);
    const rsetex = promisify(client.setex).bind(client);
    const rget = promisify(client.get).bind(client);
    const rsismember = promisify(client.sismember).bind(client);
    const rsadd = promisify(client.sadd).bind(client); // when promisified sadd the function can't accept any arguments.
                                                       // using client.sadd 

    const redisCache = {
        set(key: string, value: any){
            return rset(key, value)
            .then((val)=>val)
            .catch((err)=>err);
        },

        setTemporary(key: string, expery: number, value: any){
            return rsetex(key, expery, value)
            .then((val)=>val)
            .catch((err)=>err);
        },
    
        get(key: string){
            return rget(key)
            .then((val)=>val)
            .catch((err)=>err);
        },

        addSet(setName: string, member: string){
            return client.sadd(setName, member);
            // .then((val)=>val)
            // .catch((err)=>err);
        },
        isSetMember(setName: string, key: string){
            return rsismember(setName, key)
            .then((val)=>{
                return val
            })
            .catch((err)=>err);
        }
    }

    return redisCache;
}
