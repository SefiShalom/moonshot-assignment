import mongoose from "mongoose";
import 'dotenv/config';
import {ProcessConfigLoader} from './env';


class MongoDBConnection {

    public static async initConnection() {
        ProcessConfigLoader.Load('./.env')
        process.env.DB_CONN_STR = `mongodb://${process.env.DB_IP}:${process.env.DB_PORT}/${process.env.DB_DB_NAME}`;
        await MongoDBConnection.connect(process.env.DB_CONN_STR);
    }

    public static async connect(connStr: string) {
       return mongoose.connect(
            connStr,
            {useNewUrlParser: true, useFindAndModify: false, },
        )
            .then(() => {
                console.log(`Successfully connected to ${connStr}`);
            })
            .catch((error) => {
                console.error("Error connecting to database: ", error);
                return process.exit(1);
            });
    }

    public static setAutoReconnect() {
        mongoose.connection.on("disconnected", () => MongoDBConnection.initConnection());
    }

    public static async disconnect() {
       await mongoose.connection.close();
    }
}

export { MongoDBConnection }