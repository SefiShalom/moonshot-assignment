import mongoose, {Document, Schema} from "mongoose";
import { IDomainData } from './interfaces';

const DomainDataSchema: Schema = new Schema({
    domain: {type: String, required: true},
    browser: {type: Object, required: true},
    createdAt: {type: Date, default: new Date(),
    }
}, {strict: false});

  
export default mongoose.model<IDomainData>("DomainData", DomainDataSchema);
