import { injectable } from "inversify";
import { Error, default as mongoose } from "mongoose";
import  DomainData from "./domain.data.model";
import {ILooseObject, IPaging, IDatesRange, IDomainData, IDomainDataDBService} from './interfaces';

@injectable()
export class DomainDataMongoDBService implements IDomainDataDBService{
    
    public async getDomainData(website: string, paging: IPaging, datesRange: IDatesRange): Promise<IDomainData[]> {
    
        let query: ILooseObject = {domain: website};
        let datesRangeQuery = this.getDateRangeQuery(datesRange);
        
        if(Object.keys(datesRangeQuery).length ){ // has any date range?
            query['createdAt'] = datesRangeQuery;
        }

        let domainata = await DomainData.find(query, {}, paging);

        return domainata;
    }
    public async storeDomainData(data: IDomainData): Promise<IDomainData> {
        
        return DomainData.create(data)
            .then((data: IDomainData) => {
                return data;
            })
            .catch((error: Error) => {
                throw error;
            });
    }

    private getDateRangeQuery(range: IDatesRange){
        
        let rangeQuery: ILooseObject = {};

        if(range.from){
            rangeQuery.$gte = new Date(Date.parse(range.from));
        }

        if(range.until){
            rangeQuery.$lte = new Date(Date.parse(range.until));
        }

        return rangeQuery;
    }

}

