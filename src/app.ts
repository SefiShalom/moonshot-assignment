import "reflect-metadata";
import * as bodyParser from 'body-parser';
import express from 'express';
import {IController} from './interfaces';
import { ProcessConfigLoader } from './env';

class App {
    public app: express.Application;
   
    constructor(controllers: IController[]) {
      ProcessConfigLoader.Load('.env');
      this.app = express();
      this.initializeMiddlewares();
      this.initializeControllers(controllers);
    }
   
    public listen() {
      this.app.listen(process.env.PORT, () => {
        console.log(`App listening on the port ${process.env.PORT}`);
      });
    }
   
    private initializeMiddlewares() {
      this.app.use(bodyParser.json());
    }
   
    private initializeControllers(controllers: IController[]) {
      controllers.forEach((controller) => {
        this.app.use('/', controller.router);
      });
    }

  }
   
  export { App };